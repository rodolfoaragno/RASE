
#Class SVG for drawing/ploting

class SVG():
	def __init__(self, width=500.0, height=100.0, limits=[0,0]):
		self.points=[]
		self.width=width
		self.height=height
	def add_point(self, x, y):
		self.points.append([x,y])
	def draw(self, fig ):
		html='<svg width="{}" height="{}">'.format(self.width, self.height)
		ending="</svg>"
		print "Drawed"
		return html + fig + ending
	def draw_as_line(self, style="fill:none;stroke:black;stroke-width:1", scale=True ):
		if scale: #decide si mantener la escala original o hacer fit
			self.scale_to_img()
		else:
			self.scale_to_draw()
		string='<polyline points="'
		for i in self.points:
			string+="{},{} ".format(i[0], i[1])
		string+='" style="{}"></polyline>'.format(style)
		return self.draw(string)
	def scale_to_img(self, margin=0.1):
		x_max=max(self.points, key=lambda x: x[0])[0] #+ margin
		y_max=max(self.points, key=lambda x: x[1])[1] + margin
		x_min=min(self.points, key=lambda x: x[0])[0] #- margin
		y_min=min(self.points, key=lambda x: x[1])[1] - margin
		print "max {} -- {}".format(x_max, y_max)
		print "min {} -- {}".format(x_min, y_min)
		new_xs=map(lambda x: (x[0] - x_min)*self.width/(x_max - x_min), self.points)
		new_ys=map(lambda x:self.height - (x[1] - y_min)*self.height/(y_max - y_min), self.points)
		self.points= map(None, new_xs, new_ys)
	def scale_to_draw(self):
		self.width=max(self.points, lambda x: x[0])[0]
		self.height=max(self.points, lambda x: x[1])[1]

