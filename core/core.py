#core contiene funciones matematicas generales que devuelven
#arrays de numeros y una clase que los almacena, ponele

#tambien tiene todos los imports matematicos
#import matplotlib.pylab as plt
import numpy as np
#from scipy import signal
#from sympy import *

#plt.ion()

from svg import SVG
from codes import *
import codes

class Core():
	def __init__(self, data): # y es el array de valores a transformar
		data=dict(data)
		print data
		self.data=data;
		self.pulse=[];
		print "encoding" + self.data['code'][0]
		self.keep0=self.data.get("keep0")
		self.code=self.data.get("code")[0]
		self.min_hz=float(self.data.get("hz_medio_min")[0])
		self.max_hz=float(self.data.get("hz_medio_max")[0])
		self.sig_hz=float(self.data.get("hz_signal")[0])
		self.points=int(self.data.get("points")[0])
		#self.encode(self.data['code'][0]);
		#self.transform();#make fourier
	def set_x(self, x1, x2, hz):
		step= 1.0/ ((x2 - x1)*hz)
		self.x=np.arange(x1, x2, step);
		
	def encode(self):#cambiar por un diccionario o funcion menos peligrosa!
		print "{}({}, {})".format(self.code, self.data['signal'][0], self.data['points'][0])
		if self.code in codes.__dict__:
			self.pulse=eval('{}("{}", "{}")'.format( self.code, self.data['signal'][0], self.points))
		else:
			print "no code found"
			return
	def transform(self):
		self.pulse_old=self.pulse
		self.pulse=map(None,np.fft.fftfreq(len(self.pulse))*self.sig_hz*self.points ,np.fft.fft(map(lambda x:x[1],self.pulse)));
		l=int(len(self.pulse)/2)
		self.pulse=self.pulse[0:l]
		l=int(len(self.pulse_old)/2)
		self.pulse_old=self.pulse_old[0:l]
		if not self.keep0:
			self.pulse[0]=(0,0)
			#self.pulse_old=self.pulse_old[1:]
		#print self.pulse
		#self.pulse=filter(lambda (x, y):  (x>=self.min_hz and x<=self.max_hz), self.pulse)
		self.pulse=map(lambda (x, y):  (x, (x>=self.min_hz and x<=self.max_hz)*y), self.pulse)
		return 1
	def antitransform(self):
		#l=len(self.pulse)/4
		print "Atrans"
		#self.pulse=map(None,map(lambda x: x[0], self.pulse_old),np.fft.ifft(map(lambda x:x[1],self.pulse)));
		self.pulse=map(None,range(0, len(self.pulse)),np.fft.ifft(map(lambda x:x[1],self.pulse)));
		print self.pulse
		return 1
	def plot(self, limit=False):
		s=SVG()
		x=0
		print self.pulse
		for i in self.pulse:
			if (limit and i[0]>=self.min_hz and i[0]>=self.max_hz ):
				pass
			else:
				s.add_point(i[0] , (i[1] + 0j).real)
			#x+=1
		print "drawing"
		return s.draw_as_line()

if (__name__=="__main__"):
	eng= Core({'hz_signal': [u''], 'ih': [u''], 'code': [u'nz'], 'hz_medio': [u''], 'signal': [u'11010010122212122220000'], 'iw': [u''],'points': [u'1']});
	eng.transform()
	eng.plot()
	
	
