#!./venv/bin/python


print "hello world"

from flask import Flask
from flask import request
from flask import url_for,redirect,render_template

import types

from core.core import Core
import core.codes

codes_names = [a for a in dir(core.codes) if isinstance(core.codes.__dict__.get(a), types.FunctionType)]

app = Flask(__name__)

#hello is testing grownd !
@app.route('/hello', methods=['GET', 'POST'])
@app.route('/hello_<name>')
def hello_world(name=""):
    if name=="" :
	aca=request.args.get('name');
	if aca==None:
		return """<form action=>
Name:
<input type="text" name="name"><br>
<input type="submit">
</form>
	plug a name on the url!"""
	else:
		return redirect(url_for('hello_world', name=aca))
    return 'Hello World!' + name


#Later on everything will have it's own file... later on

@app.route('/')
@app.route('/simulator')
@app.route('/simulator_<hash_sim>')
def simulator(hash_sim=""):
	if hash_sim=="":
		return redirect(url_for('show_controls'));
	else:
		return render_template('simulator.html',  arg1=hash_sim)


@app.route('/controls')
def show_controls():
	return render_template('controls.html', route=url_for('render'), codes=codes_names);

@app.route('/render', methods=['GET', 'POST'])
def render():
	#sanitize request.form
	#execute engine
	eng= Core(request.form)
	eng.encode()
	tx1=eng.plot()
	eng.transform()
	spec1=eng.plot(True)
	eng.antitransform()
	rx1=eng.plot()
	return show_controls() + render_template('render.html',  tx=tx1, spec=spec1, rx=rx1)


if __name__ == '__main__':
    app.debug=True
    app.run()

